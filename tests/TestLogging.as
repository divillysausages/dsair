package  
{
	import com.divillysausages.dsair.DSAir;
	import com.divillysausages.dsair.log.LogManager;
	import flash.display.Sprite;
	
	/**
	 * Tests the logging capabilities of the framework.
	 * 
	 * Warning and error messages are always kept, while log messages are reused as needed
	 * @author Damian Connolly
	 */
	public class TestLogging extends Sprite 
	{
		
		public function TestLogging() 
		{
			// init the framework
			DSAir.init( this );
			
			// we can go through the logger manager
			LogManager.instance.log( this, "This is a log message" );
			LogManager.instance.warn( this, "This is a warning" );
			LogManager.instance.error( this, "This is an error" );
			
			// or use the convenience functions in DSAir
			DSAir.log( this, "This is a log message from the DSAir class" );
			DSAir.warn( this, "This is a warning message from the DSAir class" );
			DSAir.error( this, "This is an error message from the DSAir class", false );
			
			// if we want to log an error message and also show an alert, pass true
			// as the last param to DSAir.error()
			DSAir.error( this, "This error will open an alert", true );
			
			// press control+shift+l to see the logger, or use the LogManager.instance.visible property
			// when the log display is open, you can use pg-up and pg-down to move the log up and down,
			// and ctrl+c to copy the log to paste somewhere else
		}
		
	}

}