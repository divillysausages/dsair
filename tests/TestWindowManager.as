package  
{
	import com.bit101.components.PushButton;
	import com.divillysausages.dsair.DSAir;
	import com.divillysausages.dsair.windows.Alert;
	import com.divillysausages.dsair.windows.ProgressAlert;
	import com.divillysausages.dsair.windows.WindowManager;
	import flash.display.NativeWindow;
	import flash.display.NativeWindowInitOptions;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.ui.Keyboard;
	
	/**
	 * Test opening new windows with the window manager
	 * @author Damian Connolly
	 */
	public class TestWindowManager extends Sprite
	{
		
		public function TestWindowManager() 
		{
			// init the framework
			DSAir.init( this );
			
			// closing the main window will close all other windows
			
			// bind space to open a new default window
			DSAir.bindKey( Keyboard.SPACE, this._createDefault );
			
			// bind control to open a specific window
			DSAir.bindKey( Keyboard.CONTROL, this._createSpecific );
			
			// bind control to get a specific window
			DSAir.bindKey( Keyboard.SHIFT, this._centerWindow );
			
			// bind control to create an alert window
			DSAir.bindKey( Keyboard.A, this._createAlert );
			
			// bind controlt to create a progress window
			DSAir.bindKey( Keyboard.P, this._createProgress );
		}
		
		// called when we press space - create a default window
		private function _createDefault( e:KeyboardEvent ):void
		{
			// open a new window
			// this will create a default window indented slightly from the top left and not focused
			WindowManager.instance.createWindow( null, null, -1.0, -1.0, 0.0, 0.0, true, false );
			
			// get the current number of windows that we have open
			trace( "We have " + WindowManager.instance.numWindows + " windows active" );
		}
		
		// called when we press control - create a special window
		private function _createSpecific( e:KeyboardEvent ):void
		{
			// create the options so it's not resizable
			var options:NativeWindowInitOptions = new NativeWindowInitOptions;
			options.maximizable	= false;
			options.minimizable	= false;
			options.resizable	= true;
			
			// creates a specific window with focus
			WindowManager.instance.createWindow( options, "Specific", 0.0, 0.0, 320, 100, true, true );
		}
		
		// called when we press shift - center the special window
		private function _centerWindow( e:KeyboardEvent ):void
		{
			// get the speicific window
			var window:NativeWindow = WindowManager.instance.getWindowByTitle( "Specific" );
			if ( window == null )
				return;
				
			// center the window on the screen or the app
			var ran:Number = Math.random();
			if ( ran < 0.5 )
			{
				trace( "Centering the specific window on the app" );
				WindowManager.instance.centerWindowOnApp( window );
			}
			else
			{
				trace( "Centering the specific window on the screen" );
				WindowManager.instance.centerWindowOnScreen( window );
			}
		}
		
		// called when we press a - creates an alert window
		private function _createAlert( e:KeyboardEvent ):void
		{
			// create our alert window
			var alert:Alert = new Alert( "Some title", "This is the message to show" );
			
			// add a button (all buttons close the alert)
			alert.addButton( "Close this", this._onClose );
			
			// add a disabled button
			alert.addButton( "Can't click this", null, false );
			
			// show it
			alert.show();
		}
		
		// called when we click the close button on the alert
		private function _onClose():void
		{
			trace( "Closed the alert!" );
		}
		
		// called when we press p - create a progress window
		private var m_progress:ProgressAlert 	= null;
		private var m_progressOK:PushButton		= null;
		private function _createProgress( e:KeyboardEvent ):void
		{
			// create our progress alert
			this.m_progress = new ProgressAlert( "Something's happening", "blah blah" );
			
			// add the onComplete listener
			this.m_progress.onComplete = this._onProgressComplete;
			
			// optionally hide it automatically on complete
			this.m_progress.closeOnComplete = false;
			
			// add some buttons
			this.m_progress.addButton( "Cancel", this._onCloseProgress );
			this.m_progressOK = this.m_progress.addButton( "Can't click", null, false );
			
			// show it
			this.m_progress.show();
			
			// add an event listener to update the progress
			this.addEventListener( Event.ENTER_FRAME, this._onEnterFrame );
		}
		
		// called when we get to 100% complete
		private function _onProgressComplete():void
		{
			trace( "Complete!" );
			
			// remove the enter frame event listener
			this.removeEventListener( Event.ENTER_FRAME, this._onEnterFrame );
			
			// enable the ok button
			this.m_progressOK.label		= "Can click!";
			this.m_progressOK.enabled 	= true;
		}
		
		// called when we close the progress bar using the button
		private function _onCloseProgress():void
		{
			trace( "Closed the progress window!" );
			
			// remove the enter frame event listener
			this.removeEventListener( Event.ENTER_FRAME, this._onEnterFrame );
		}
		
		// called every frame the progress alert is opened
		private function _onEnterFrame( e:Event ):void
		{
			if ( this.m_progress != null )
				this.m_progress.progress += 0.01;
		}
		
	}

}