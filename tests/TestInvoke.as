package  
{
	import com.divillysausages.dsair.DSAir;
	import com.divillysausages.dsair.invoke.InvokeManager;
	import flash.display.Sprite;
	import flash.filesystem.File;
	
	/**
	 * Tests the invoke manager, which is starting the app by dragging a file
	 * over it, or if the app is pinned in windows, right-clicking on the icon
	 * and selecting a file that we've pinned to the app, or starting the app
	 * through the command line
	 * 
	 * To test this, you need to make an app.
	 * @author Damian Connolly
	 */
	public class TestInvoke extends Sprite 
	{
		
		public function TestInvoke() 
		{
			// init the framework
			DSAir.init( this );
			
			// check if we have some files
			trace( "Where we started with any files: " + InvokeManager.instance.hasFiles );
			
			// get the files that we were started with
			for each( var file:File in InvokeManager.instance.files )
				trace( "We were started with the file '" + file.nativePath + "'" );
				
			// if our arguments weren't files, we can still access them in their raw form
			for each( var arg:String in InvokeManager.instance.rawArgs )
				trace( this, "Started with raw arg '" + arg + "'" );
		}
		
	}

}