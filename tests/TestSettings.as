package  
{
	import com.divillysausages.dsair.DSAir;
	import com.divillysausages.dsair.settings.SettingsManager;
	import flash.display.Sprite;
	import flash.events.KeyboardEvent;
	import flash.ui.Keyboard;
	
	/**
	 * Tests saving and loading some settings
	 * @author Damian Connolly
	 */
	public class TestSettings extends Sprite 
	{
		
		public function TestSettings() 
		{
			// init the framework
			DSAir.init( this );
			
			// set the class that we're using to save the setting if we want to
			// keep a particular class. If that class has another class as a 
			// parameter, then register that as well
			SettingsManager.instance.registerClass( MySettings );
			
			// check to we have some settings
			var settings:MySettings = SettingsManager.instance.get() as MySettings;
			trace( "Do we have any settings: " + settings );
			
			// if we don't have settings, create some
			if ( settings == null )
				settings = new MySettings;
				
			// increase our param
			settings.someParam++;
			
			// save it
			var success:Boolean = SettingsManager.instance.save( settings );
			trace( "Did we save our settings alright? " + success );
			
			// bind a key so we can clear the settings
			DSAir.bindKey( Keyboard.SPACE, this._onSpace );
		}
		
		// called when we press space - clear the settings
		private function _onSpace( e:KeyboardEvent ):void
		{
			trace( "Clearing settings" );
			SettingsManager.instance.clear();
		}
		
	}

}

// the class that we'll use to store our settings
internal class MySettings
{
	/**
	 * Some random var
	 */
	public var someParam:int = 0;
	
	/**
	 * A String version of the class for when it's traced etc
	 */
	public function toString():String
	{
		return "[MySettings someParam: " + this.someParam + "]";
	}
}