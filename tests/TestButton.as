package  
{
	import com.divillysausages.dsair.DSAir;
	import com.divillysausages.dsair.input.Button;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	
	/**
	 * The test file for showing how the Button class works
	 * @author Damian Connolly
	 */
	public class TestButton extends Sprite
	{
		private var m_txtButton:Button 		= null; // button using a textfield
		private var m_spriteButton:Button	= null; // button using a sprite
		
		public function TestButton() 
		{
			// init the framework
			DSAir.init( this );
			
			// create the text button
			var t:TextField = new TextField;
			t.autoSize		= TextFieldAutoSize.LEFT;
			t.border		= true;
			t.text			= "TextField button";
			this.m_txtButton = new Button( t, this._onClickTextButton );
			
			// create the sprite button
			var s:Sprite = new Sprite;
			s.graphics.beginFill( 0xff0000 );
			s.graphics.drawCircle( 0.0, 0.0, 30.0 );
			s.graphics.endFill();
			this.m_spriteButton = new Button( s ); // no onclick callback as that's after
			this.m_spriteButton.addEventCallbacks( this._onClickSpriteButton, this._onOver, this._onOut );
			
			// you can also use a MovieClip as the graphics. If it has the frames
			// "over", "out", or "down" specified, then it will automatically
			// gotoAndStop() at the right frame
			
			// position them (we can access x, y, width and height)
			this.m_txtButton.x 		= 20.0;
			this.m_spriteButton.x	= this.m_txtButton.x + this.m_txtButton.width + this.m_spriteButton.width * 0.5 + 20.0;
			this.m_txtButton.y		= this.m_spriteButton.y = 100.0;
			
			// add them (use the graphics property)
			this.addChild( this.m_txtButton.graphics );
			this.addChild( this.m_spriteButton.graphics );
		}
		
		// called when we click on the text button (no parameters)
		private function _onClickTextButton():void
		{
			trace( "Clicked on the textfield button, toggling the sprite button" );
			this.m_spriteButton.enabled = !this.m_spriteButton.enabled;
		}
		
		// called when we click on the sprite button (Button parameter)
		private function _onClickSpriteButton( b:Button ):void
		{
			trace( "Did we click on the sprite button? " + ( b == this.m_spriteButton ) );
		}
		
		// called when we mouse over the sprite button
		private function _onOver():void
		{
			trace( "On over the sprite button" );
		}
		
		// called when we mouse out of the sprite button
		private function _onOut():void
		{
			trace( "On out for the sprite button" );
		}
		
	}

}