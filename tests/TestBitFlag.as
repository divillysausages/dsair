package  
{
	import com.divillysausages.dsair.DSAir;
	import com.divillysausages.dsair.util.BitFlag;
	import flash.display.Sprite;
	
	/**
	 * Tests the BitFlag class. If you don't know what bitflags are, then check out
	 * http://en.wikipedia.org/wiki/Bit_field
	 * 
	 * You can read more about this class at
	 * http://divillysausages.com/blog/a_bitflag_class_for_as3
	 * 
	 * @author Damian Connolly
	 */
	public class TestBitFlag extends Sprite 
	{
		
		public function TestBitFlag() 
		{
			// init the framework
			DSAir.init( this );
			
			// tests a normal flag
			this._testNormalFlag();
			
			// tests a specific flag (checking)
			this._testSpecificFlag();
		}
		
		// tests a normal flag where we can add any flag
		private function _testNormalFlag():void
		{
			trace( "\nTesting with a generic bit flag" );
			
			// create a normal bit flag
			var flag:BitFlag = new BitFlag;
			
			// add a flag
			flag.addFlag( 1 << 1 );
			
			// or add multiple flags
			flag.addFlags( 1 << 2, 1 << 3, 1 << 4 );
			
			// check if we have flags
			trace( "Do we have the (1 << 1) flag: " + flag.hasFlag( 1 << 1 ) );
			trace( "Do we have the (1 << 1) and (1 << 2) flags: " + flag.hasFlags( 1 << 1, 1 << 2 ) );
			trace( "Do we have the (1 << 1) and (1 << 5) flags: " + flag.hasFlags( 1 << 1, 1 << 5 ) );
			trace( "Do we have the (1 << 1) OR (1 << 5) flags: " + flag.hasAnyFlag( 1 << 1, 1 << 5 ) );
			
			// remove a flag
			flag.removeFlag( 1 << 1 );
			trace( "Do we have the (1 << 1) flag: " + flag.hasFlag( 1 << 1 ) );
			
			// toggle a flag
			flag.toggleFlag( 1 << 1 );
			trace( "Do we have the (1 << 1) flag: " + flag.hasFlag( 1 << 1 ) );
		}
		
		// tests a specific bit flag (with a class for testing)
		private function _testSpecificFlag():void
		{
			trace( "\nTesting with a specific class to check against" );
			
			// create a flag with a specified class to check against
			var flag:BitFlag = new BitFlag( MyFlags );
			
			// add a flag
			flag.addFlag( MyFlags.ONE );
			
			// check
			trace( "Do we have the ONE flag: " + flag.hasFlag( MyFlags.ONE ) );
			
			// add a flag that's not in our class
			flag.addFlag( 1 << 4 );
			trace( "Do we have the (1 << 4) flag: " + flag.hasFlag( 1 << 4 ) );
		}
		
	}

}

internal class MyFlags
{
	public static const ONE:int 				= 1 << 1; // can be an int
	public static const TWO:uint 				= 1 << 2; // or an uint
	public static const IGNORED_STRING:String 	= "random"; // ignored as not a flag
	public static const IGNORED_INT:int 		= 25; // ignored as not a flag
	public static const IGNORED_NUMBER:Number 	= 1.0; // ignored as not an int or uint
	public static const TOO_LARGE:int 			= 1 << 31; // ignored as it's too big
}