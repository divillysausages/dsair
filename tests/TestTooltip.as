package  
{
	import com.divillysausages.dsair.DSAir;
	import com.divillysausages.dsair.util.Tooltip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	/**
	 * Tests the tooltip class of the framework
	 * @author Damian Connolly
	 */
	public class TestTooltip extends Sprite
	{
		private var m_clicks:int = 0; // the number of times we've clicked the graphic
		
		public function TestTooltip() 
		{
			// init the framework
			DSAir.init( this );
			
			// create our object
			var s:Sprite = new Sprite;
			s.graphics.beginFill( 0xff0000 );
			s.graphics.drawCircle( 0.0, 0.0, 20.0 );
			s.graphics.endFill();
			s.x = s.y = 50.0;
			this.addChild( s );
			
			// add a click listener to it
			s.addEventListener( MouseEvent.CLICK, this._onClick );
			
			// add a tooltip to the graphic
			Tooltip.instance.add( s, "Click once to change the tooltip" );
		}
		
		// called when we click on the graphic
		private function _onClick( e:MouseEvent ):void
		{
			// get our graphic
			var s:Sprite = e.target as Sprite;
			
			// up the clicks
			this.m_clicks++;
			if ( this.m_clicks == 1 )
				Tooltip.instance.add( s, "When the tooltip changes, it doesn't disappear onClick" );
			else if ( this.m_clicks == 3 )
				Tooltip.instance.add( s, "Click once more to remove the tooltip completely" );
			else if ( this.m_clicks > 3 )
				Tooltip.instance.remove( s );
		}
		
	}

}