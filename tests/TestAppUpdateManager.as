package  
{
	import com.divillysausages.dsair.appupdate.AppUpdateManager;
	import com.divillysausages.dsair.DSAir;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	
	/**
	 * The test file for testing how the app updater works
	 * @author Damian Connolly
	 */
	public class TestAppUpdateManager extends Sprite
	{
		
		public function TestAppUpdateManager() 
		{
			// init the framework
			DSAir.init( this );
			
			// is this the first run after an update
			trace( "Is this the first run after an update: " + AppUpdateManager.instance.isFirstRun );
			
			// create the button to generate the update xml file
			var btn:TextField 	= new TextField;
			btn.text			= "Generate update XML file";
			btn.autoSize		= TextFieldAutoSize.LEFT;
			btn.selectable		= false;
			btn.border			= true;
			btn.background		= true;
			btn.x = btn.y		= 10.0;
			btn.addEventListener( MouseEvent.CLICK, this._onClickBtn );
			this.addChild( btn );
			
			// optionally hide the "No update is available alert" if we're up-to-date
			AppUpdateManager.instance.showNoUpdateAvailableAlert = true;
			
			// check for an update
			AppUpdateManager.instance.checkForUpdate( "http://127.0.0.1/dsair/update.xml" );
		}
		
		// called when we click the button to generate the update xml
		private function _onClickBtn( e:MouseEvent ):void
		{
			// generate the file
			AppUpdateManager.instance.generateUpdateXML( "http://127.0.0.1/dsair/DSAir.air", "The notes for this version", "1.0.2", false );
		}
		
	}

}