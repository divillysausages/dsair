package  
{
	import com.divillysausages.dsair.DSAir;
	import com.divillysausages.dsair.input.InputManager;
	import flash.display.Sprite;
	import flash.events.KeyboardEvent;
	import flash.ui.Keyboard;
	
	/**
	 * Tests the input options
	 * @author Damian Connolly
	 */
	public class TestInput extends Sprite
	{
		
		public function TestInput() 
		{
			// init the framework
			DSAir.init( this );
			
			// we can bind directly using through the input manager
			InputManager.instance.bind( Keyboard.SPACE, this._onSpace );
			
			// or we can use the convenience function in the DSAir class
			DSAir.bindKey( Keyboard.CONTROL, this._onControl );
		}
		
		// called when we press the space key
		private function _onSpace( e:KeyboardEvent ):void
		{
			trace( "Pressed space!" );
			
			// we can unbind through the InputManager
			InputManager.instance.unbind( Keyboard.SPACE );
		}
		
		// called when we press the control key
		private function _onControl( e:KeyboardEvent ):void
		{
			trace( "Pressed control!" );
			
			// we can unbind through DSAir as well
			DSAir.unbindKey( Keyboard.CONTROL );
		}
		
	}

}