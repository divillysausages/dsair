package  
{
	import com.divillysausages.dsair.dragdrop.DragDropManager;
	import com.divillysausages.dsair.DSAir;
	import flash.display.Sprite;
	
	/**
	 * Tests creating an object that we can use to drag and drop files onto
	 * @author Damian Connolly
	 */
	public class TestDragDrop extends Sprite
	{
		
		public function TestDragDrop() 
		{
			// init the framework
			DSAir.init( this );
			
			// create our target
			var target:MyDropTarget = new MyDropTarget;
			target.x = target.y = 50.0;
			this.addChild( target );
			
			// set the target
			DragDropManager.instance.target = target;
		}
		
	}

}
import com.divillysausages.dsair.dragdrop.IDragDrop;
import flash.filesystem.File;

internal class MyDropTarget extends flash.display.Sprite implements IDragDrop
{
	/**
	 * Creates the MyDropTarget object
	 */
	public function MyDropTarget()
	{
		this._draw( 0xff0000 );
	}
	
	/**
	 * Called when we drag files over the target
	 */
	public function onDragOver():void 
	{
		this._draw( 0x00ff00 );
	}
	
	/**
	 * Called when we drag files out of the target
	 */
	public function onDragOut():void 
	{
		this._draw( 0xff0000 );
	}
	
	/**
	 * Called when we drop files on the target
	 * @param files The array of File objects that we dropped
	 */
	public function onDragDrop( files:Array /**File*/ ):void 
	{
		this._draw( 0x0000ff );
		for each( var file:File in files )
			trace( "File '" + file.nativePath + "' was dropped on us" );
	}
	
	// draws our graphics
	private function _draw( colour:uint ):void
	{
		this.graphics.clear();
		this.graphics.beginFill( colour );
		this.graphics.drawCircle( 0.0, 0.0, 20.0 );
		this.graphics.endFill();
	}
}