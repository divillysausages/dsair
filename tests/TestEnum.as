package  
{
	import com.divillysausages.dsair.util.Enum;
	import flash.display.Sprite;
	
	/**
	 * Tests the enum class. The Enum class is adapted from Scott Bilas' Enum class, 
	 * which you can find at
	 * http://scottbilas.com/blog/ultimate-as3-fake-enums/
	 * 
	 * @author Damian Connolly
	 */
	public class TestEnum extends Sprite
	{
		
		public function TestEnum() 
		{
			// trace the enum itself
			trace( MyEnum.SOME_ENUM_1 );
			
			// trace it's name and index
			trace( "It's name is " + MyEnum.SOME_ENUM_1.name );
			trace( "It's index is " + MyEnum.SOME_ENUM_1.index );
			
			// get all the enums for a class
			trace( "The enums for class MyEnum: " + Enum.getEnum( MyEnum ) );
			
			// find a particular enum by name (pass true to make it case-sensitive)
			trace( "The enum in MyEnum with the name 'two' is: " + Enum.getField( MyEnum, "some_enum_1", false ) );
			
			// find a particular enum by index
			trace( "The enum in MyEnum with index '2' is: " + Enum.getByIndex( MyEnum, 2 ) );
		}
		
	}

}
import com.divillysausages.dsair.util.Enum;

internal class MyEnum extends Enum
{
	// this line is important. It's a static constructor and does all
	// the magic necessary to make it work
	{ initEnum( MyEnum ) }
	
	public static const SOME_ENUM_1:MyEnum = new MyEnum;
	public static const SOME_ENUM_2:MyEnum = new MyEnum;
	public static const SOME_ENUM_3:MyEnum = new MyEnum;
}