package  
{
	import com.divillysausages.dsair.DSAir;
	import flash.display.Sprite;
	import flash.events.KeyboardEvent;
	import flash.ui.Keyboard;
	
	/**
	 * Tests the main DSAir class
	 * @author Damian Connolly
	 */
	public class TestDSAir extends Sprite
	{
		
		public function TestDSAir() 
		{
			// init the framework
			DSAir.init( this );
			
			// get the stage
			trace( "The stage: " + DSAir.stage );
			
			// the main clas
			trace( "The main class: " + DSAir.main );
			
			// the app name and version
			trace( "The app name and version: App:" + DSAir.appName + " (v" + DSAir.appVersion + ")" );
			
			// the screen res
			trace( "The screen res: " + DSAir.screenRes );
			
			// the main screen size (usable area)
			trace( "The main screen size: " + DSAir.mainScreenSize );
			
			// logging
			DSAir.log( this, "This is a log" );
			DSAir.warn( this, "This is a warning" );
			DSAir.error( this, "This is an error", false ); // true to also show an alert
			
			// binding keys
			DSAir.bindKey( Keyboard.SPACE, this._onSpace );
		}
		
		// called when we pressed space
		private function _onSpace( e:KeyboardEvent ):void
		{
			trace( "Pressed space!" );
			DSAir.unbindKey( Keyboard.SPACE );
		}
		
	}

}