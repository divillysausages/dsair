package  
{
	import com.divillysausages.dsair.DSAir;
	import com.divillysausages.dsair.throttle.ThrottleManager;
	import flash.display.Sprite;
	
	/**
	 * Tests the throttling of the framework. The fps of the app is throttled when it's
	 * not active, down to 5fps, which should be enough to keep streaming/sockets open.
	 * It's enabled by default
	 * @author Damian Connolly
	 */
	public class TestThrottle extends Sprite
	{
		
		public function TestThrottle() 
		{
			// init the framework
			DSAir.init( this );
			
			// get the current framerate of the app
			trace( "The current framerate is: " + ThrottleManager.instance.framerate );
			
			// click anywhere off the app so that it doesn't have focus in order to see
			// the throttling in action
			
			// set the current framerate
			ThrottleManager.instance.framerate = 20;
			
			// check is the app currently active
			trace( "Is the app currently active: " + ThrottleManager.instance.isAppActive );
			
			// add a callback for when the app's activity changes
			ThrottleManager.instance.onAppActiveChange = this._onChange;
		}
		
		// called when the app activates or deactivates
		private function _onChange():void
		{
			trace( "The app's activity changed, are we active: " + ThrottleManager.instance.isAppActive );
		}
		
	}

}