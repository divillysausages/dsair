package com.divillysausages.dsair.windows 
{
	import com.bit101.components.ProgressBar;
	import com.bit101.components.PushButton;
	import com.divillysausages.dsair.DSAir;
	
	/**
	 * An alert window that has a progress bar
	 * @author Damian Connolly
	 */
	public class ProgressAlert extends Alert
	{
		
		/********************************************************************/
		
		/**
		 * Should the progress alert close automatically when we're complete?
		 * Default is false
		 */
		public var closeOnComplete:Boolean = false;
		
		/********************************************************************/
		
		private var m_bar:ProgressBar 		= null; // the progress bar to show
		private var m_onComplete:Function	= null;	// the function to call when the progress is complete
		
		/********************************************************************/
		
		/**
		 * Sets the value for the progress bar
		 */
		public function get progress():Number { return ( this.m_bar != null ) ? this.m_bar.value : 0.0; }
		public function set progress( n:Number ):void
		{
			if ( this.m_bar == null )
				return;
				
			// our value
			var value:Number = ( n < 0.0 ) ? 0.0 : ( n > 1.0 ) ? 1.0 : n;
			
			// if it's the same, do nothing
			if ( value == this.m_bar.value )
				return;
			this.m_bar.value = value;
			
			// if we're at 100%, call our callback
			if ( this.m_bar.value == 1.0 && this.m_onComplete != null )
			{
				this.m_onComplete();
				if ( this.closeOnComplete )
					this.hide();
			}
		}
		
		/**
		 * Sets the function to call when the progress bar has reached 100%. It should take no parameters
		 */
		public function set onComplete( f:Function ):void
		{
			if ( f != null && f.length != 0 )
			{
				DSAir.error( this, "The onComplete callback takes no parameters" );
				return;
			}
			this.m_onComplete = f;
		}
		
		/********************************************************************/
		
		/**
		 * Creates a new Progress Alert window
		 * @param title The title of the window
		 * @param msg The message to show
		 */
		public function ProgressAlert( title:String, msg:String ) 
		{
			super( title, msg );
			
			// create our bar
			this.m_bar 			= new ProgressBar( this.m_window.stage, this.m_padding, this.m_msg.y + this.m_msg.height + this.m_padding );
			this.m_bar.width	= this.m_window.stage.stageWidth - this.m_padding * 2.0;
			
			// resize the alert height
			this._resizeWindow();
		}
		
		/**
		 * Destroys the progress alert and clears it for garbage collection
		 */
		override public function destroy():void 
		{
			// kill our bar
			if( this.m_bar != null )
				this.m_bar.destroy();
			
			// null our properties
			this.m_bar 			= null;
			this.m_onComplete	= null;
			
			// super it
			super.destroy();
		}
		
		/**
		 * Adds a button to the progress alert
		 * @param label The label for the button
		 * @param onClick The function to call when we click the button
		 * @param enabled Is the button enabled or not?
		 * @return The PushButton created
		 */
		override public function addButton( label:String, onClick:Function = null, enabled:Boolean = true ):PushButton 
		{
			// create the button
			var btn:PushButton = super.addButton( label, onClick, enabled );
			
			// move the button below the bar
			btn.y = this.m_bar.y + this.m_bar.height + this.m_padding;
			
			// resize the alert height
			this.m_window.stage.stageHeight = btn.y + btn.height + this.m_padding;
			
			// resize the bar if needed
			this.m_bar.width = this.m_window.stage.stageWidth - this.m_padding * 2.0;
			
			// return the button
			return btn;
		}
		
		/********************************************************************/
		
		/**
		 * Resizes the window so the text and buttons all fit properly
		 */
		override protected function _resizeWindow():void
		{
			if ( this.m_buttons.length > 0 || this.m_bar == null )
				super._resizeWindow();
			else
				this.m_window.stage.stageHeight = this.m_bar.y + this.m_bar.height + this.m_padding;
		}
		
	}

}