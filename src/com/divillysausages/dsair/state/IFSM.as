package com.divillysausages.dsair.state 
{
	
	/**
	 * The interface that describes a finite state machine
	 * @author Damian Connolly
	 */
	public interface IFSM 
	{
		
		/********************************************************************/
		
		/**
		 * The current state that we're in. Will be null if we're not in any state
		 */
		function get currState():IState;
		
		/**
		 * The next state that we're going to. Will be null if we're not in middle of
		 * changing states.
		 */
		function get nextState():IState;
		
		/********************************************************************/
		
		/**
		 * Destroys the IFSM and clears it for garbage collection
		 */
		function destroy():void;
		
		/**
		 * Adds a state to the state machine
		 * @param name The name that we want to add the state under
		 * @param state The state that we're adding
		 * @return If the state was addded
		 */
		function addState( name:String, state:IState ):Boolean;
		
		/**
		 * Returns an IState based on a name
		 * @param name The name of the IState that we're looking for
		 * @return The IState, or null if we can't find it
		 */
		function getState( name:String ):IState;
		
		/**
		 * Change the current state
		 * @param name The name of the IState that we want to change to
		 * @param data Any data that we want to pass to the IState
		 * @return If it worked. This will return false if we're already in that state, for example
		 */
		function changeState( name:String, data:* = null ):Boolean;
	}
	
}