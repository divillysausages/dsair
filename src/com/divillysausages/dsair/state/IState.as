package com.divillysausages.dsair.state 
{
	
	/**
	 * An interface for a state that can be used with a IStateManager
	 * @author Damian Connolly
	 */
	public interface IState 
	{
		
		/********************************************************************/
		
		/**
		 * The name of this IState
		 */
		function get name():String;
		function set name( s:String ):void;
		
		/**
		 * The IFSM that's controlling us
		 */
		function get manager():IFSM;
		function set manager( m:IFSM ):void;
		
		/**
		 * Has this state been fully started?
		 */
		function get isStarted():Boolean;
		
		/**
		 * Has this state been fully stopped?
		 */
		function get isStopped():Boolean;
		
		/********************************************************************/
		
		/**
		 * Destroys the IState and clears it for garbage collection
		 */
		function destroy():void;
		
		/**
		 * The function called when we want to start this state
		 */
		function start():void;
		
		/**
		 * The function called when this state has started
		 * @param data Any data that is passed to this state when starting
		 */
		function onStart( data:* = null ):void;
		
		/**
		 * The function called when we want to stop this state
		 */
		function stop():void;
		
		/**
		 * The function called when this state has stopped
		 */
		function onStop():void;
	}
	
}