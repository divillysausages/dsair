package com.divillysausages.dsair.state 
{
	/**
	 * A basic implementation of the IState interface, for use with the StateManager
	 * @author Damian Connolly
	 */
	public class State implements IState
	{
		
		/********************************************************************/
		
		protected var m_name:String		= null; // the name of this state
		protected var m_manager:IFSM 	= null; // the manager that's controlling us
		protected var m_started:Boolean	= false;// has this state been fully started
		protected var m_stopped:Boolean	= false;// has this state been fully stopped
		
		/********************************************************************/
		
		/**
		 * The name of this State
		 */
		public function get name():String { return this.m_name; }
		public function set name( s:String ):void
		{
			this.m_name = s;
		}
		
		/**
		 * The IFSM that's controlling us
		 */
		public function get manager():IFSM { return this.m_manager; }
		public function set manager( m:IFSM ):void
		{
			this.m_manager = m;
		}
		
		/**
		 * Has this state been fully started?
		 */
		public function get isStarted():Boolean { return this.m_started; }
		
		/**
		 * Has this state been fully stopped?
		 */
		public function get isStopped():Boolean { return this.m_stopped; }
		
		/********************************************************************/
		
		/**
		 * Destroys the State and clears it for garbage collection
		 */
		public function destroy():void
		{
			this.m_manager = null;
		}
		
		/**
		 * The function called when we want to start this state
		 */
		public function start():void
		{
			this.m_stopped	= false;
			this.m_started 	= true;
		}
		
		/**
		 * The function called when this state has started
		 * @param data Any data that is passed to this state when starting
		 */
		public function onStart( data:* = null ):void
		{
			// override this
		}
		
		/**
		 * The function called when we want to stop this state
		 */
		public function stop():void
		{
			this.m_started	= false;
			this.m_stopped	= true;
		}
		
		/**
		 * The function called when this state has stopped
		 */
		public function onStop():void
		{
			// override this
		}
		
	}

}