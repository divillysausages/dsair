package com.divillysausages.dsair.log 
{
	import com.divillysausages.dsair.util.Enum;
	
	/**
	 * The different levels of logs that you can have
	 * @author Damian Connolly
	 */
	public class LogLevel extends Enum
	{
		
		/********************************************************************/
		
		// static ctor - parse the enum
		{ initEnum( LogLevel ); }
		
		/********************************************************************/
		
		/**
		 * The level for debug messages
		 */
		public static const DEBUG:LogLevel = new LogLevel;
		
		/**
		 * The level for normal messages
		 */
		public static const MSG:LogLevel = new LogLevel;
		
		/**
		 * The level for warning messages
		 */
		public static const WARN:LogLevel = new LogLevel;
		
		/**
		 * The level for error messages
		 */
		public static const ERROR:LogLevel = new LogLevel;
		
	}

}