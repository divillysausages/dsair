package com.divillysausages.dsair.log 
{
	/**
	 * A log message that we can display later if there's a need
	 * @author Damian Connolly
	 */
	public class LogMsg
	{		
		/********************************************************************/
		
		/**
		 * The actual message
		 */
		public var msg:String = null;
		
		/**
		 * The type of message that this is
		 */
		public var type:LogLevel = LogLevel.DEBUG;
		
		/**
		 * The timestamp for the message
		 */
		public var timestamp:Date = null;
		
		/********************************************************************/
		
		/**
		 * Inits our LogMsg object
		 * @param message The message that we want to store
		 * @param type The type of the message
		 */
		public function init( message:String, type:LogLevel ):void
		{
			// store our timestamp
			this.timestamp = new Date;
			
			// store our type
			this.type = type;
			
			// store our message
			this.msg = message;
		}
		
		/**
		 * Returns the LogMsg as a String
		 * @return The String representation of the LogMsg object
		 */
		public function toString():String
		{
			return this.timestamp.toTimeString() + ": [" + this.type.name + "] - " + this.msg;
		}
		
		/**
		 * Returns the LogMsg as a HTML String
		 * @return The HTML String representation of the LogMsg object
		 */
		public function toHTMLSTring():String
		{
			if ( this.type == LogLevel.DEBUG )
				return "<p><font face='Courier Sans' size='10' color='#666666'>" + this + "</font></p>";
			else if ( this.type == LogLevel.MSG )
				return "<p><font face='Courier Sans' size='10'>" + this + "</font></p>";
			else if ( this.type == LogLevel.WARN )
				return "<p><font face='Courier Sans' size='10' color='#999900'>" + this + "</font></p>";
			else
				return "<p><font face='Courier Sans' size='10' color='#990000'>" + this + "</font></p>";
				
			// failsafe
			return this;
		}
		
	}

}