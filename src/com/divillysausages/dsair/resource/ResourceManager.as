package com.divillysausages.dsair.resource 
{
	/**
	 * The ResourceManager controls the different resources etc that we have in our app
	 * @author Damian Connolly
	 */
	public class ResourceManager 
	{
		
		/********************************************************************/
		
		/**
		 * The font that we've embedded
		 */
		public static const FONT:String = "PF Ronda Seven";
		
		/********************************************************************/
		
		private static var m_instance:ResourceManager	= null;	// the only instance of the resource manager
		private static var m_creating:Boolean			= false;// are we creating the resource manager?
		
		/********************************************************************/
		
		/**
		 * The singleton instance for the resource manager
		 */
		public static function get instance():ResourceManager
		{
			if ( ResourceManager.m_instance == null )
			{
				ResourceManager.m_creating = true;
				ResourceManager.m_instance = new ResourceManager;
				ResourceManager.m_creating = false;
			}
			return ResourceManager.m_instance;
		}
		
		/********************************************************************/
		
		// The font we'll use for our components
		[Embed(source="../../../../../assets/pf_ronda_seven.ttf", embedAsCFF="false", fontName="PF Ronda Seven", mimeType="application/x-font")]
		private var m_ronda:Class;
		
		/********************************************************************/
		
		/**
		 * Creates a ResourceManager. As ResourceManager is a singleton, this shouldn't be called
		 * directly, but rather the static instance getter instead
		 */
		public function ResourceManager() 
		{
			if ( !ResourceManager.m_creating )
				throw new Error( "ResourceManager is a singleton. Use the static instance getter instead" );
		}
		
	}

}