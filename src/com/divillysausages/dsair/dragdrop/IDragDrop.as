package com.divillysausages.dsair.dragdrop 
{
	import flash.events.IEventDispatcher;
	
	/**
	 * The interface that classes wanting to use the DragDropManager must implement
	 * @author Damian Connolly
	 */
	public interface IDragDrop extends IEventDispatcher
	{
		/**
		 * The function that gets called when a file is dragged over the object
		 */
		function onDragOver():void;
		
		/**
		 * The function that gets called when a file is dragged out of the object
		 */
		function onDragOut():void;
		
		/**
		 * The function that gets called when we drop our files onto the object
		 * @param files The array of File objects that were dragged in
		 */
		function onDragDrop( files:Array /**File*/ ):void;
	}
	
}