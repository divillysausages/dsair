package com.divillysausages.dsair.invoke 
{
	import com.divillysausages.dsair.DSAir;
	import flash.desktop.NativeApplication;
	import flash.events.InvokeEvent;
	import flash.filesystem.File;
	
	/**
	 * The invoke manager adds a listener to the app to see if it was started by someone dragging
	 * a file over the icon or similar
	 * @author Damian Connolly
	 */
	public class InvokeManager 
	{
		
		/********************************************************************/
		
		private static var m_instance:InvokeManager	= null;	// the only instance of the invoke manager
		private static var m_creating:Boolean		= false;// are we creating the invoke manager?
		
		/********************************************************************/
		
		/**
		 * The singleton instance for the invoke manager
		 */
		public static function get instance():InvokeManager
		{
			if ( InvokeManager.m_instance == null )
			{
				InvokeManager.m_creating = true;
				InvokeManager.m_instance = new InvokeManager;
				InvokeManager.m_creating = false;
			}
			return InvokeManager.m_instance;
		}
		
		/********************************************************************/
		
		/**
		 * The files that were passed in when the app was invoked, if any
		 */
		public var files:Vector.<File> = new Vector.<File>;
		
		/**
		 * The raw arguments that were passed in
		 */
		public var rawArgs:Vector.<String> = new Vector.<String>;
		
		/********************************************************************/
		
		/**
		 * Returns if the InvokeManager has files or not
		 */
		public function get hasFiles():Boolean { return ( this.files.length > 0 ); }
		
		/********************************************************************/
		
		/**
		 * Creates an InvokeManager. As InvokeManager is a singleton, this shouldn't be called
		 * directly, but rather the static instance getter instead
		 */
		public function InvokeManager() 
		{
			if ( !InvokeManager.m_creating )
				throw new Error( "InvokeManager is a singleton. Use the static instance getter instead" );
				
			// add our event listener
			NativeApplication.nativeApplication.addEventListener( InvokeEvent.INVOKE, this._onInvoke );
		}
		
		/********************************************************************/
		
		// called when the app is started - check if it was invoked with a specific file
		private function _onInvoke( e:InvokeEvent ):void
		{
			// if there's no arguments, do nothing
			if ( e.arguments.length == 0 )
				return;
				
			// go through the arguments and get the strings
			for each( var arg:String in e.arguments )
			{
				// add it to our raw arguments
				this.rawArgs.push( arg );
				
				// try to create a file
				try
				{
					var file:File = new File( arg ); // can throw an error
					if ( !file.exists )
						DSAir.warn( this, "The argument passed (" + arg + ") isn't a valid file" );
					else
						this.files.push( file );
				}
				catch ( e:Error ) { } // it wasn't a valid file path
			}
		}
		
	}

}