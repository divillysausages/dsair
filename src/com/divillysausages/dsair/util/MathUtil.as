package com.divillysausages.dsair.util
{
	import flash.geom.Point;
	
	/**
	 * Common math utils
	 * @author Damian Connolly
	 */
	public class MathUtil 
	{
		
		/**
		 * A number used for comparing number within a specific range
		 */
		public static const EPSILON:Number = 0.0000001;
		
		/********************************************************************/
		
		private static const D_TO_R:Number = Math.PI / 180.0; // easy, quick conversion from degrees to radians
		private static const R_TO_D:Number = 180.0 / Math.PI; // easy, quick conversion from radians to degrees
		
		/********************************************************************/
		
		/**
		 * Returns true if the number is equal to zero (within a range)
		 * @param n		The number of check
		 * @param range	Our range. If the number is less than range and greater than -range, then this returns true
		 * @return		If it's close to zero or not
		 */
		public static function isEqualZero( n:Number, range:Number = MathUtil.EPSILON ):Boolean
		{
			return ( n >= -range && n <= range );
		}
		
		/**
		 * Returns the radian angle of a Point
		 * @param p		The Point that we're checking the angle of
		 * @return		The angle in radians
		 */
		public static function radianAngle( p:Point ):Number
		{
			return Math.atan2( p.y, p.x );
		}
		
		/**
		 * Converts a given degree value to its radian equivalent
		 * @param deg	The degree value that we want to change
		 * @return		The radian value of deg
		 */
		public static function degToRad( deg:Number ):Number
		{
			return deg * MathUtil.D_TO_R;
		}
		
		/**
		 * Converts a given radian value to its degree equivalent
		 * @param rad	The radian value that we want to change
		 * @return		The degree value of rad
		 */
		public static function radToDeg( rad:Number ):Number
		{
			return rad * MathUtil.R_TO_D;
		}
		
		/**
		 * Clamps a value between 2 given ranges
		 * @param value		The value to clamp
		 * @param min		The min value allowed
		 * @param max		The max value allowed
		 * @return	The clamped value
		 * @see clampUint
		 */
		public static function clamp( value:Number, min:Number, max:Number ):Number
		{
			return ( value < min ) ? min : ( value > max ) ? max : value;
		}
	}
}